$(function() {
    $('nav a').on('click', function() {
        $('#' + $(this).attr('href').substring(1)).fadeIn(100).siblings().fadeOut(50);
        return false;
    });
    $(function() {
        $('.fansy').imageLightbox();
    });
    $('.close').on('click', function() {
        $('section').fadeOut(50);
        return false;
    });
})
